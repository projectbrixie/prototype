git pull
kill $(jps | grep prototype-0.0.1.jar | awk '{print $1}')
./gradlew bootJar
kill $(ps -aux | grep gradle |grep -v grep | awk '{print $2}')
sleep 5
nohup java -Xms2048m -Xmx2048m -jar ./build/libs/prototype-0.0.1.jar &
