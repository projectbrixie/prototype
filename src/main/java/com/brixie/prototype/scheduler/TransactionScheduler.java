package com.brixie.prototype.scheduler;

import com.brixie.prototype.data.PropertyData;
import com.brixie.prototype.data.PropertyType;
import com.brixie.prototype.data.TransactionData;
import com.brixie.prototype.service.GoogleSheetService;
import com.brixie.prototype.service.ManipulationConfigurationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.brixie.prototype.utils.RandomUtils.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionScheduler {

    private static final String VALUE_INPUT_OPTION = "USER_ENTERED";

    private final GoogleSheetService service;
    private final ManipulationConfigurationService configurationService;

    // add a property
    @Scheduled(cron = "*/30 * * * * *")
    public void generateProperty() throws GeneralSecurityException, IOException {
        if (configurationService.getEnabledScheduler()) {
            log.info("================ property scheduler is running ================");
            final var configuration = configurationService.readConfiguration();
            var originalValue = nextBigDecimal(configuration.getMinPropertyOriginalValue().doubleValue(),
                    configuration.getMaxPropertyOriginalValue().doubleValue());
            var currentValue = nextBigDecimal(configuration.getMinPropertyCurrentValue().doubleValue(),
                    configuration.getMaxPropertyCurrentValue().doubleValue());
            var numberOfToken = nextInt(configuration.getMinPropertyNumberOfToken(), configuration.getMaxPropertyNumberOfToken());
            var currentNftTokenValue = nextBigDecimal(configuration.getMinPropertyCurrentNftTokenValue().doubleValue(),
                    configuration.getMaxPropertyCurrentNftTokenValue().doubleValue());
            var sheetId = configurationService.getSheetIdBySheetName(configuration.getGoogleSheetId());

            final var lastPropertyId = service.readLastId(sheetId);
            final var property = new PropertyData()
                    .setId(lastPropertyId)
                    .setPropertyType(randomEnum(PropertyType.class))
                    .setOriginalValue(originalValue)
                    .setCurrentValue(currentValue)
                    .setNumberOfToken(numberOfToken)
                    .setCurrentNftTokenValue(currentNftTokenValue);
            final List<Object> propertyNewRowInput = List.of(property.getId(), property.getPropertyType().toString(), property.getOriginalValue(),
                    property.getCurrentValue(), property.getNumberOfToken(), property.getCurrentNftTokenValue());
            final List<List<Object>> propertyValues = List.of(
                    propertyNewRowInput
            );
            service.appendValues(sheetId, "Properties!A1", VALUE_INPUT_OPTION, propertyValues);
            log.info("================ property scheduler is finished ================");
        }
    }

    // add a transaction
    @Scheduled(cron = "*/5 * * * * *")
    public void generateTransaction() throws GeneralSecurityException, IOException {
        if (configurationService.getEnabledScheduler()) {
            log.info("***************** property transaction scheduler is running *****************");
            final var configuration = configurationService.readConfiguration();
            var sheetId = configurationService.getSheetIdBySheetName(configuration.getGoogleSheetId());
            var lastPropertyId = service.readLastId(sheetId) - 1;
            if (lastPropertyId < 1) {
                return;
            }
            var numberOfToken = nextInt(configuration.getMinTransactionNumberOfToken(),
                    configuration.getMaxTransactionNumberOfToken());
            var tokenPrice = nextBigDecimal(configuration.getMinTransactionPrice().doubleValue(),
                    configuration.getMaxTransactionPrice().doubleValue());
            var saleTotal = BigDecimal.valueOf(numberOfToken).multiply(tokenPrice);
            var brixieFee = Optional.of(saleTotal.multiply(configuration.getBrixieFee()))
                    .filter(fee -> fee.compareTo(BigDecimal.ONE) > 0)
                    .orElse(BigDecimal.ONE)
                    .setScale(2, RoundingMode.DOWN);
            var cardFee = Optional.of(saleTotal.multiply(configuration.getCardFee()))
                    .filter(fee -> fee.compareTo(BigDecimal.ONE) > 0)
                    .orElse(BigDecimal.ONE)
                    .setScale(2, RoundingMode.DOWN);
            final var transaction = new TransactionData()
                    .setDate(LocalDate.now())
                    .setNumberOfToken(numberOfToken)
                    .setTransactionPrice(tokenPrice)
                    .setAsset(lastPropertyId)
                    .setSaleTotal(saleTotal)
                    .setBrixieFee(brixieFee)
                    .setCardFee(cardFee);
            final List<Object> transactionNewRowInput = List.of(
                    transaction.getDate().toString(),
                    transaction.getNumberOfToken(),
                    transaction.getTransactionPrice(),
                    transaction.getAsset(),
                    transaction.getSaleTotal(),
                    transaction.getBrixieFee(),
                    transaction.getCardFee());
            final List<List<Object>> transactionValues = List.of(
                    transactionNewRowInput
            );
            service.appendValues(sheetId, "Transaction!A1", VALUE_INPUT_OPTION, transactionValues);
            log.info("***************** property transaction scheduler is finished *****************");
        }
    }
}
