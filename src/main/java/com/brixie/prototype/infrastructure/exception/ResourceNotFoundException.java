package com.brixie.prototype.infrastructure.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(Class<?> clazz, final Long id) {
        super(String.format("reason: cannot find resource type: %s by identifier: %s", clazz.getSimpleName(), id));
    }

    public ResourceNotFoundException(Class<?> clazz, final String name) {
        super(String.format("reason: cannot find resource type: %s by name: %s", clazz.getSimpleName(), name));
    }
}
