package com.brixie.prototype.infrastructure.exception;

import lombok.Getter;

import java.util.Collections;
import java.util.List;

@Getter
public class PlatformApiDataValidationException extends RuntimeException {

    private final String message;
    private final List<Object> errors;

    public PlatformApiDataValidationException(final List<Object> errors) {
        super("Validation error");
        this.message = super.getMessage();
        this.errors = errors;
    }

    public PlatformApiDataValidationException(final String error) {
        super("Validation error");
        this.message = super.getMessage();
        this.errors = Collections.singletonList(error);
    }

    public PlatformApiDataValidationException(final String message, final List<Object> errors) {
        super(message);
        this.message = message;
        this.errors = errors;
    }

    public PlatformApiDataValidationException(final String message, final String error) {
        super(message);
        this.message = message;
        this.errors = Collections.singletonList(error);
    }
}
