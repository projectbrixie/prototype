package com.brixie.prototype.infrastructure.exception;

import com.brixie.prototype.infrastructure.domain.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ApiError> handleResourceNotFoundException(final ResourceNotFoundException ex) {
        List<Object> errors = new ArrayList<>();
        errors.add(ex.getMessage());
        final var error = new ApiError("Record Not Found", errors);
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PlatformApiDataValidationException.class)
    public final ResponseEntity<ApiError> handlePlatformApiDataValidationException(final PlatformApiDataValidationException ex) {
        final var error = new ApiError(ex.getMessage(), ex.getErrors());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
