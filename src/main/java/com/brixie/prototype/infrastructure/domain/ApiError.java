package com.brixie.prototype.infrastructure.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Getter
@Setter
public class ApiError {

    private String message;
    private List<Object> errors;

    public ApiError(String message, List<Object> errors) {
        this.message = message;
        this.errors = errors;
    }

    public ApiError(String message, Object error) {
        this.message = message;
        errors = Collections.singletonList(error);
    }
}
