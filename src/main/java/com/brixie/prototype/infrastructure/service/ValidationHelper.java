package com.brixie.prototype.infrastructure.service;

public interface ValidationHelper {

    void entityValidation(final Object object);
}
