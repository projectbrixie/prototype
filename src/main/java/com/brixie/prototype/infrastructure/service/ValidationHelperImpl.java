package com.brixie.prototype.infrastructure.service;

import com.brixie.prototype.infrastructure.exception.PlatformApiDataValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ValidationHelperImpl implements ValidationHelper{

    private final Validator validator;

    @Override
    public void entityValidation(final Object entity) {
        final List<Object> constraintViolationList = new ArrayList<>();
        final Set<ConstraintViolation<Object>> constraintViolationSet = validator.validate(entity);
        if (!constraintViolationSet.isEmpty()) {
            constraintViolationSet.forEach(constraintViolation ->
                    constraintViolationList.add(constraintViolation.getPropertyPath() + "| " + constraintViolation.getMessage()));
        }
        if (constraintViolationList.size() > 0) {
            throw new PlatformApiDataValidationException(constraintViolationList);
        }
    }
}
