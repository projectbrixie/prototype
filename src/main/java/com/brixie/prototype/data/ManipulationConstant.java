package com.brixie.prototype.data;

public class ManipulationConstant {
    public static final String PREFIX_KEY = "config";
    public static final String KEY_ENABLED_SCHEDULER = PREFIX_KEY + ":enabled-scheduler";
    public static final String KEY_MIN_PROPERTY_ORIGINAL_VALUE = PREFIX_KEY + ":min-property-original-value";
    public static final String KEY_MAX_PROPERTY_ORIGINAL_VALUE = PREFIX_KEY + ":max-property-original-value";
    public static final String KEY_MIN_PROPERTY_CURRENT_VALUE = PREFIX_KEY + ":min-property-current-value";
    public static final String KEY_MAX_PROPERTY_CURRENT_VALUE = PREFIX_KEY + ":max-property-current-value";
    public static final String KEY_MIN_PROPERTY_NUMBER_OF_TOKEN = PREFIX_KEY + ":min-property-number-of-token";
    public static final String KEY_MAX_PROPERTY_NUMBER_OF_TOKEN = PREFIX_KEY + ":max-property-number-of-token";
    public static final String KEY_MIN_PROPERTY_CURRENT_NFT_TOKEN_VALUE = PREFIX_KEY + ":min-property-current-nft-token-value";
    public static final String KEY_MAX_PROPERTY_CURRENT_NFT_TOKEN_VALUE = PREFIX_KEY + ":max-property-current-nft-token-value";
    // transaction
    public static final String KEY_MIN_TRANSACTION_NUMBER_OF_TOKEN = PREFIX_KEY + ":min-transaction-number-of-token";
    public static final String KEY_MAX_TRANSACTION_NUMBER_OF_TOKEN = PREFIX_KEY + ":max-transaction-number-of-token";
    public static final String KEY_MIN_TRANSACTION_PRICE = PREFIX_KEY + ":min-transaction-price";
    public static final String KEY_MAX_TRANSACTION_PRICE = PREFIX_KEY + ":max-transaction-price";
    public static final String KEY_TRANSACTION_BRIXIE_FEE = PREFIX_KEY + ":transaction-brixie-fee";
    public static final String KEY_TRANSACTION_CARD_FEE = PREFIX_KEY + ":transaction-card-fee";

    // google sheet
    public static final String KEY_GOOGLE_SHEET_ID = PREFIX_KEY + ":google-sheet-id";
    public static final String KEY_GOOGLE_SHEET = PREFIX_KEY + ":google-sheet";
}
