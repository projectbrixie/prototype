package com.brixie.prototype.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class TransactionData {

    private LocalDate date;
    private Integer numberOfToken;
    private BigDecimal transactionPrice;
    private Long asset ;
    private BigDecimal saleTotal;
    private BigDecimal brixieFee;
    private BigDecimal cardFee;
}
