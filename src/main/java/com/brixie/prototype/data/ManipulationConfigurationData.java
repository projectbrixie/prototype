package com.brixie.prototype.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
@Accessors(chain = true)
public class ManipulationConfigurationData {

    @NotNull
    private BigDecimal minPropertyOriginalValue;
    @NotNull
    private BigDecimal maxPropertyOriginalValue;
    @NotNull
    private BigDecimal minPropertyCurrentValue;
    @NotNull
    private BigDecimal maxPropertyCurrentValue;
    @NotNull
    private Integer minPropertyNumberOfToken;
    @NotNull
    private Integer maxPropertyNumberOfToken;
    @NotNull
    private BigDecimal minPropertyCurrentNftTokenValue;
    @NotNull
    private BigDecimal maxPropertyCurrentNftTokenValue;
    // transaction
    @NotNull
    private Integer minTransactionNumberOfToken;
    @NotNull
    private Integer maxTransactionNumberOfToken;
    @NotNull
    private BigDecimal minTransactionPrice;
    @NotNull
    private BigDecimal maxTransactionPrice;
    @NotNull
    private BigDecimal brixieFee;
    @NotNull
    private BigDecimal cardFee;
    @NotNull
    @NotBlank
    private String googleSheetId;
}
