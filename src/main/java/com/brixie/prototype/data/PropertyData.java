package com.brixie.prototype.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class PropertyData {
    private Long id;
    private PropertyType propertyType;
    private BigDecimal originalValue;
    private BigDecimal currentValue;
    private Integer numberOfToken;
    private BigDecimal currentNftTokenValue;
}
