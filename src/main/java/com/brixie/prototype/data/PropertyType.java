package com.brixie.prototype.data;

public enum PropertyType {
    House,
    Land,
    Apartment
}