package com.brixie.prototype.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@ToString
@Accessors(chain = true)
public class GoogleSheetData {

    @NotNull
    @NotBlank
    private String sheetName;

    @NotNull
    @NotBlank
    private String sheetId;
}
