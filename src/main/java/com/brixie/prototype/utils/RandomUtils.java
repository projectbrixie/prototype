package com.brixie.prototype.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class RandomUtils {

    private static final Random RANDOM = new Random();

    public static <T extends Enum<?>> T randomEnum(final Class<T> clazz) {
        final var x = RANDOM.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    public static BigDecimal nextBigDecimal(Double from, Double to) {
        return BigDecimal.valueOf(RANDOM.doubles(1, from, to)
                        .findAny()
                        .orElse(1))
                .setScale(2, RoundingMode.DOWN);
    }

    public static Integer nextInt(Integer from, Integer to) {
        return RANDOM.ints(1, from, to)
                .findAny()
                .orElse(1);
    }
}
