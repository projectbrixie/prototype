package com.brixie.prototype.service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class GoogleSheetService {

    private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    //private static final String SHEET_ID = "1BTwiuVlKWTU25qs3htDmwXZ9FlysvxY9rdnyOGnpL60";
    private static final List<String> SCOPES = List.of(
            SheetsScopes.SPREADSHEETS,
            SheetsScopes.SPREADSHEETS_READONLY,
            SheetsScopes.DRIVE,
            SheetsScopes.DRIVE_READONLY,
            SheetsScopes.DRIVE_FILE);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    public void appendValues(final String sheetId,
                             final String range,
                             final String valueInputOption,
                             final List<List<Object>> values) throws IOException, GeneralSecurityException {
        final var httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        final var service = new Sheets.Builder(httpTransport, JSON_FACTORY, getCredentials(httpTransport))
                .setApplicationName(APPLICATION_NAME)
                .build();
        final var body = new ValueRange()
                .setValues(values);
        service.spreadsheets().values().append(sheetId, range, body)
                .setValueInputOption(valueInputOption)
                .execute();
    }

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        final var inputStream = GoogleSheetService.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (inputStream == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        final var clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(inputStream));

        // Build flow and trigger user authorization request.
        final var flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        final var receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    public Long readLastId(final String sheetId) throws IOException, GeneralSecurityException {
        // Build a new authorized API client service.
        final var HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final var range = "Properties!A1:D";
        final var service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        final var response = service.spreadsheets().values()
                .get(sheetId, range)
                .execute();
        final List<List<Object>> values = response.getValues();
        if (values == null || values.isEmpty()) {
            return 1L;
        } else {
            try {
                return Long.parseLong(values.get(values.size() - 1).get(0).toString()) + 1;
            } catch (final NumberFormatException e) {
                return 1L;
            }
        }
    }
}
