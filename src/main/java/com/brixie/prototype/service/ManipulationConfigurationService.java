package com.brixie.prototype.service;

import com.brixie.prototype.data.GoogleSheetData;
import com.brixie.prototype.data.ManipulationConfigurationData;
import com.brixie.prototype.infrastructure.service.ValidationHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

import static com.brixie.prototype.data.ManipulationConstant.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class ManipulationConfigurationService {

    private final StringRedisTemplate redisTemplate;
    private final ValidationHelper validationHelper;

    public void updateConfiguration(final ManipulationConfigurationData requestBody) {
        validationHelper.entityValidation(requestBody);
        final var valueOperation = redisTemplate.opsForValue();
        // property
        valueOperation.set(KEY_MIN_PROPERTY_ORIGINAL_VALUE, requestBody.getMinPropertyOriginalValue().toString());
        valueOperation.set(KEY_MAX_PROPERTY_ORIGINAL_VALUE, requestBody.getMaxPropertyOriginalValue().toString());
        valueOperation.set(KEY_MIN_PROPERTY_CURRENT_VALUE, requestBody.getMinPropertyCurrentValue().toString());
        valueOperation.set(KEY_MAX_PROPERTY_CURRENT_VALUE, requestBody.getMaxPropertyCurrentValue().toString());
        valueOperation.set(KEY_MIN_PROPERTY_NUMBER_OF_TOKEN, requestBody.getMinPropertyNumberOfToken().toString());
        valueOperation.set(KEY_MAX_PROPERTY_NUMBER_OF_TOKEN, requestBody.getMaxPropertyNumberOfToken().toString());
        valueOperation.set(KEY_MIN_PROPERTY_CURRENT_NFT_TOKEN_VALUE, requestBody.getMinPropertyCurrentNftTokenValue().toString());
        valueOperation.set(KEY_MAX_PROPERTY_CURRENT_NFT_TOKEN_VALUE, requestBody.getMaxPropertyCurrentNftTokenValue().toString());
        // transaction
        valueOperation.set(KEY_MIN_TRANSACTION_NUMBER_OF_TOKEN, requestBody.getMinTransactionNumberOfToken().toString());
        valueOperation.set(KEY_MAX_TRANSACTION_NUMBER_OF_TOKEN, requestBody.getMaxTransactionNumberOfToken().toString());
        valueOperation.set(KEY_MIN_TRANSACTION_PRICE, requestBody.getMinTransactionPrice().toString());
        valueOperation.set(KEY_MAX_TRANSACTION_PRICE, requestBody.getMaxTransactionPrice().toString());
        valueOperation.set(KEY_TRANSACTION_BRIXIE_FEE, requestBody.getBrixieFee().toString());
        valueOperation.set(KEY_TRANSACTION_CARD_FEE, requestBody.getCardFee().toString());
        valueOperation.set(KEY_GOOGLE_SHEET_ID, requestBody.getGoogleSheetId());
        log.info("configuration has been updated");
    }

    public ManipulationConfigurationData readConfiguration() {
        final var valueOperation = redisTemplate.opsForValue();
        final var minPropertyOriginalValue = valueOperation.get(KEY_MIN_PROPERTY_ORIGINAL_VALUE);
        final var maxPropertyOriginalValue = valueOperation.get(KEY_MAX_PROPERTY_ORIGINAL_VALUE);
        final var minPropertyCurrentValue = valueOperation.get(KEY_MIN_PROPERTY_CURRENT_VALUE);
        final var maxPropertyCurrentValue = valueOperation.get(KEY_MAX_PROPERTY_CURRENT_VALUE);
        final var minPropertyNumberOfToken = valueOperation.get(KEY_MIN_PROPERTY_NUMBER_OF_TOKEN);
        final var maxPropertyNumberOfToken = valueOperation.get(KEY_MAX_PROPERTY_NUMBER_OF_TOKEN);
        final var minPropertyCurrentNftTokenValue = valueOperation.get(KEY_MIN_PROPERTY_CURRENT_NFT_TOKEN_VALUE);
        final var maxPropertyCurrentNftTokenValue = valueOperation.get(KEY_MAX_PROPERTY_CURRENT_NFT_TOKEN_VALUE);
        // transaction
        final var minTransactionNumberOfToken = valueOperation.get(KEY_MIN_TRANSACTION_NUMBER_OF_TOKEN);
        final var maxTransactionNumberOfToken = valueOperation.get(KEY_MAX_TRANSACTION_NUMBER_OF_TOKEN);
        final var minTransactionPrice = valueOperation.get(KEY_MIN_TRANSACTION_PRICE);
        final var maxTransactionPrice = valueOperation.get(KEY_MAX_TRANSACTION_PRICE);
        final var brixieFee = valueOperation.get(KEY_TRANSACTION_BRIXIE_FEE);
        final var cardFee = valueOperation.get(KEY_TRANSACTION_CARD_FEE);

        final var googleSheetId = valueOperation.get(KEY_GOOGLE_SHEET_ID);

        return new ManipulationConfigurationData()
                .setMinPropertyOriginalValue(minPropertyOriginalValue == null ? BigDecimal.ZERO : new BigDecimal(minPropertyOriginalValue))
                .setMaxPropertyOriginalValue(maxPropertyOriginalValue == null ? BigDecimal.ZERO : new BigDecimal(maxPropertyOriginalValue))
                .setMinPropertyCurrentValue(minPropertyCurrentValue == null ? BigDecimal.ZERO : new BigDecimal(minPropertyCurrentValue))
                .setMaxPropertyCurrentValue(maxPropertyCurrentValue== null ? BigDecimal.ZERO : new BigDecimal(maxPropertyCurrentValue))
                .setMinPropertyNumberOfToken(minPropertyNumberOfToken == null ? 0 : Integer.parseInt(minPropertyNumberOfToken))
                .setMaxPropertyNumberOfToken(maxPropertyNumberOfToken == null ? 0 : Integer.parseInt(maxPropertyNumberOfToken))
                .setMinPropertyCurrentNftTokenValue(minPropertyCurrentNftTokenValue== null ? BigDecimal.ZERO : new BigDecimal(minPropertyCurrentNftTokenValue))
                .setMaxPropertyCurrentNftTokenValue(maxPropertyCurrentNftTokenValue == null ? BigDecimal.ZERO : new BigDecimal(maxPropertyCurrentNftTokenValue))
                .setMinTransactionNumberOfToken(minTransactionNumberOfToken == null ? 0 :Integer.parseInt(minTransactionNumberOfToken))
                .setMaxTransactionNumberOfToken(maxTransactionNumberOfToken == null ? 0 : Integer.parseInt(maxTransactionNumberOfToken))
                .setMinTransactionPrice(minTransactionPrice== null ? BigDecimal.ZERO : new BigDecimal(minTransactionPrice))
                .setMaxTransactionPrice(maxTransactionPrice== null ? BigDecimal.ZERO : new BigDecimal(maxTransactionPrice))
                .setBrixieFee(brixieFee== null ? BigDecimal.ZERO : new BigDecimal(brixieFee))
                .setCardFee(cardFee== null ? BigDecimal.ZERO : new BigDecimal(cardFee))
                .setGoogleSheetId(googleSheetId);
    }

    public boolean enableOrDisableScheduler() {
        final var currentValue = getEnabledScheduler();
        final var valueOperation = redisTemplate.opsForValue();
        final var updateValue = !currentValue;
        valueOperation.set(KEY_ENABLED_SCHEDULER, String.valueOf(updateValue));
        return updateValue;
    }

    public boolean getEnabledScheduler() {
        final var valueOperation = redisTemplate.opsForValue();
        final var enabledScheduler = valueOperation.get(KEY_ENABLED_SCHEDULER);
        if (enabledScheduler == null) {
            return false;
        }
        return Boolean.parseBoolean(enabledScheduler);
    }

    public void addGoogleSheet(final GoogleSheetData googleSheet) {
        validationHelper.entityValidation(googleSheet);
        final var name = googleSheet.getSheetName();
        final var id = googleSheet.getSheetId();

        final var opsForHash = redisTemplate.opsForHash();
        opsForHash.put(KEY_GOOGLE_SHEET, name, id);
    }

    public void deleteGoogleSheet(final String name) {
        final var opsForHash = redisTemplate.opsForHash();
        opsForHash.delete(KEY_GOOGLE_SHEET, name);
    }

    public Map<Object, Object> getAllGoogleSheet() {
        final var opsForHash = redisTemplate.opsForHash();
        return opsForHash.entries(KEY_GOOGLE_SHEET);
    }

    public String getSheetIdBySheetName(final String name) {
        final var opsForHash = redisTemplate.opsForHash();
        return String.valueOf(opsForHash.get(KEY_GOOGLE_SHEET, name));
    }
}
