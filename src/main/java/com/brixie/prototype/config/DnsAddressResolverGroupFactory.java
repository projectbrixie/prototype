package com.brixie.prototype.config;

import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.resolver.AddressResolverGroup;
import io.netty.resolver.ResolvedAddressTypes;
import io.netty.resolver.dns.DnsAddressResolverGroup;
import io.netty.resolver.dns.DnsNameResolverBuilder;
import io.netty.resolver.dns.DnsServerAddressStreamProvider;
import io.netty.resolver.dns.DnsServerAddressStreamProviders;
import org.redisson.connection.AddressResolverGroupFactory;

import java.net.InetSocketAddress;

public class DnsAddressResolverGroupFactory implements AddressResolverGroupFactory {
    @Override
    public AddressResolverGroup<InetSocketAddress> create(final Class<? extends DatagramChannel> channelType,
                                                          final DnsServerAddressStreamProvider nameServerProvider) {
        return new DnsAddressResolverGroup(new DnsNameResolverBuilder()
                .channelType(NioDatagramChannel.class)
                .nameServerProvider(DnsServerAddressStreamProviders.platformDefault())
                .resolvedAddressTypes(ResolvedAddressTypes.IPV4_ONLY));
    }
}
