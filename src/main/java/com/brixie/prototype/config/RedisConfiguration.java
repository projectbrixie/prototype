package com.brixie.prototype.config;

import lombok.RequiredArgsConstructor;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.spring.data.connection.RedissonConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
@RequiredArgsConstructor
public class RedisConfiguration {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private Integer port;

    @Value("${spring.redis.database}")
    private Integer database;

    @Bean
    public RedissonClient redisson() {
        final var config = new Config();
        config.setAddressResolverGroupFactory(new DnsAddressResolverGroupFactory())
                .useSingleServer()
                .setAddress(String.format("redis://%s:%s", host, port))
                .setDatabase(database);
        return Redisson.create(config);
    }

    @Bean
    public RedissonConnectionFactory redissonConnectionFactory(final RedissonClient redisson) {
        return new RedissonConnectionFactory(redisson);
    }

    @Bean
    public StringRedisTemplate redisTemplate(final RedissonClient redisson) {
        return new StringRedisTemplate(redissonConnectionFactory(redisson));
    }

}