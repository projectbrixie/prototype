package com.brixie.prototype.api;

import com.brixie.prototype.data.GoogleSheetData;
import com.brixie.prototype.data.ManipulationConfigurationData;
import com.brixie.prototype.service.ManipulationConfigurationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RequestMapping("/manipulation")
@RestController
@RequiredArgsConstructor
public class ManipulationController {

    private final ManipulationConfigurationService service;

    @PutMapping("/configuration")
    public void updateConfigurationParameter(@RequestBody final ManipulationConfigurationData requestBody) {
        service.updateConfiguration(requestBody);
    }

    @GetMapping("/configuration")
    public ManipulationConfigurationData getConfigurationParameter() {
        return service.readConfiguration();
    }

    @PutMapping("/configuration/enable-disable")
    public Map<String, Boolean> updateConfiguration() {
        final var result = service.enableOrDisableScheduler();
        return Map.of("enabled", result);
    }

    @GetMapping("/configuration/enable-disable")
    public Map<String, Boolean> getConfiguration() {
        final var result = service.getEnabledScheduler();
        return Map.of("enabled", result);
    }

    @PostMapping("/configuration/google-sheet")
    public void addGoogleSheetId(@RequestBody final GoogleSheetData googleSheetData) {
        service.addGoogleSheet(googleSheetData);
    }

    @DeleteMapping("/configuration/google-sheet/{name}")
    public void deleteGoogleSheetId(@PathVariable("name") final String name) {
        service.deleteGoogleSheet(name);
    }

    @GetMapping("/configuration/google-sheet")
    public Map<Object, Object> getAllGoogleSheet() {
        return service.getAllGoogleSheet();
    }
}